<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\core\PcasGrafikRef */

$this->title = 'Create Pcas Grafik Ref';
$this->params['breadcrumbs'][] = ['label' => 'Pcas Grafik Refs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pcas-grafik-ref-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
