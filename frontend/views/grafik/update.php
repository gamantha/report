<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\core\PcasGrafikRef */

$this->title = 'Update Pcas Grafik Ref: ' . $model->grafik;
$this->params['breadcrumbs'][] = ['label' => 'Pcas Grafik Refs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->grafik, 'url' => ['view', 'id' => $model->grafik]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pcas-grafik-ref-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
