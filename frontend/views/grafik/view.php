<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\core\PcasGrafikRef */

$this->title = $model->grafik;
$this->params['breadcrumbs'][] = ['label' => 'Pcas Grafik Refs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pcas-grafik-ref-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->grafik], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->grafik], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'grafik',
            'di',
            'ds',
            'dc',
            'is',
            'ic',
            'sc',
            'd-pos',
            'i-pos',
            's-pos',
            'c-pos',
        ],
    ]) ?>

</div>
