<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\core\PcasGrafikRef */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pcas-grafik-ref-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'grafik')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'di')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ds')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ic')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'd-pos')->textInput() ?>

    <?= $form->field($model, 'i-pos')->textInput() ?>

    <?= $form->field($model, 's-pos')->textInput() ?>

    <?= $form->field($model, 'c-pos')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
