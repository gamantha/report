<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\core\PcasGrafikRefSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pcas-grafik-ref-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'grafik') ?>

    <?= $form->field($model, 'di') ?>

    <?= $form->field($model, 'ds') ?>

    <?= $form->field($model, 'dc') ?>

    <?= $form->field($model, 'is') ?>

    <?php // echo $form->field($model, 'ic') ?>

    <?php // echo $form->field($model, 'sc') ?>

    <?php // echo $form->field($model, 'd-pos') ?>

    <?php // echo $form->field($model, 'i-pos') ?>

    <?php // echo $form->field($model, 's-pos') ?>

    <?php // echo $form->field($model, 'c-pos') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
