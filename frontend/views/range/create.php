<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\core\PcasRangeMap */

$this->title = 'Create Pcas Range Map';
$this->params['breadcrumbs'][] = ['label' => 'Pcas Range Maps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pcas-range-map-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
