<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\core\PcasRangeMap */

$this->title = 'Update Pcas Range Map: ' . $model->grafik;
$this->params['breadcrumbs'][] = ['label' => 'Pcas Range Maps', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->grafik, 'url' => ['view', 'id' => $model->grafik]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pcas-range-map-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
