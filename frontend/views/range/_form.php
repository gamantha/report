<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\core\PcasRangeMap */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pcas-range-map-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'grafik')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'group')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dmin')->textInput() ?>

    <?= $form->field($model, 'dmax')->textInput() ?>

    <?= $form->field($model, 'imin')->textInput() ?>

    <?= $form->field($model, 'imax')->textInput() ?>

    <?= $form->field($model, 'smin')->textInput() ?>

    <?= $form->field($model, 'smax')->textInput() ?>

    <?= $form->field($model, 'cmin')->textInput() ?>

    <?= $form->field($model, 'cmax')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
