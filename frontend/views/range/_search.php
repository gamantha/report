<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\core\PcasRangeMapSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pcas-range-map-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'grafik') ?>

    <?= $form->field($model, 'group') ?>

    <?= $form->field($model, 'dmin') ?>

    <?= $form->field($model, 'dmax') ?>

    <?= $form->field($model, 'imin') ?>

    <?php // echo $form->field($model, 'imax') ?>

    <?php // echo $form->field($model, 'smin') ?>

    <?php // echo $form->field($model, 'smax') ?>

    <?php // echo $form->field($model, 'cmin') ?>

    <?php // echo $form->field($model, 'cmax') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
