<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\core\PcasRangeMapSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pcas Range Maps';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pcas-range-map-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Pcas Range Map', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'grafik',
            'group',
            'dmin',
            'dmax',
            'imin',
            // 'imax',
            // 'smin',
            // 'smax',
            // 'cmin',
            // 'cmax',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
