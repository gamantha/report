<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\core\PcasRangeMap */

$this->title = $model->grafik;
$this->params['breadcrumbs'][] = ['label' => 'Pcas Range Maps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pcas-range-map-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->grafik], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->grafik], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'grafik',
            'group',
            'dmin',
            'dmax',
            'imin',
            'imax',
            'smin',
            'smax',
            'cmin',
            'cmax',
        ],
    ]) ?>

</div>
