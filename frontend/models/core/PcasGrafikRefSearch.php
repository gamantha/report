<?php

namespace app\models\core;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\core\PcasGrafikRef;

/**
 * PcasGrafikRefSearch represents the model behind the search form about `app\models\core\PcasGrafikRef`.
 */
class PcasGrafikRefSearch extends PcasGrafikRef
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['grafik', 'di', 'ds', 'dc', 'is', 'ic', 'sc'], 'safe'],
            [['d-pos', 'i-pos', 's-pos', 'c-pos'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PcasGrafikRef::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        $query->andFilterWhere(['like', 'grafik', $this->grafik])
            ->andFilterWhere(['like', 'di', $this->di])
            ->andFilterWhere(['like', 'ds', $this->ds])
            ->andFilterWhere(['like', 'dc', $this->dc])
            ->andFilterWhere(['like', 'is', $this->is])
            ->andFilterWhere(['like', 'ic', $this->ic])
            ->andFilterWhere(['like', 'sc', $this->sc]);

        return $dataProvider;
    }
}
