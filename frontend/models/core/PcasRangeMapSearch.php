<?php

namespace app\models\core;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\core\PcasRangeMap;

/**
 * PcasRangeMapSearch represents the model behind the search form about `app\models\core\PcasRangeMap`.
 */
class PcasRangeMapSearch extends PcasRangeMap
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['grafik', 'group'], 'safe'],
            [['dmin', 'dmax', 'imin', 'imax', 'smin', 'smax', 'cmin', 'cmax'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PcasRangeMap::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'dmin' => $this->dmin,
            'dmax' => $this->dmax,
            'imin' => $this->imin,
            'imax' => $this->imax,
            'smin' => $this->smin,
            'smax' => $this->smax,
            'cmin' => $this->cmin,
            'cmax' => $this->cmax,
        ]);

        $query->andFilterWhere(['like', 'grafik', $this->grafik])
            ->andFilterWhere(['like', 'group', $this->group]);

        return $dataProvider;
    }
}
